`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
//
// Module Name: sync_signal
//
// Description:
//   Clock domain crossing of a slow signal
//   Only for spare changes. For a too quick burst of changes, not all intermediate
//   values will be propagated. The last change will always be propagated.
//
//////////////////////////////////////////////////////////////////////////////////


module sync_signal #(
    int WIDTH  = 1,
    int STAGES = 1
  )(
    input  out_clk,
    input  [WIDTH-1 : 0] signal_in,  // i, async
    output [WIDTH-1 : 0] signal_out  // o, sync to out_clk
);

  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1 : 0] signal_sync [STAGES : 0];

  assign signal_out = signal_sync[STAGES];

  always_ff @(posedge out_clk) begin
    signal_sync[0] <= signal_in;
    for (int i=1; i <= STAGES; i=i+1) begin
      signal_sync[i] <= signal_sync[i-1];
    end
  end

endmodule
