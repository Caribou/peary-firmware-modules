`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: DESY
// Engineer: Tomas Vanat
//
// Module Name: sync_bus_with_strobe
//
// Description:
//   Synchronizes bus signal in CDC, based on a strobe signal.
//   Only for spare changes. For a too quick burst of changes, not all
//   values will be propagated. A change may not be propagated, if it comes
//   before the previous sync cycle is finished.
//
//
// Revision:
// 26.06.2023  1  T. Vanat <tomas.vanat@desy.de>  first version
//
//////////////////////////////////////////////////////////////////////////////////

module sync_bus_with_strobe #(
    int WIDTH  = 1,
    int STAGES_SIG = 1,
    int STAGES_ACK = 1
  )(
    input in_clk,
    input out_clk,
    input in_strobe,
    output out_strobe,
    input  [WIDTH-1:0] bus_in,
    output [WIDTH-1:0] bus_out
);

  logic [WIDTH-1:0] bus_in_stored = {(WIDTH){1'b0}};
  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1:0] bus_out_reg   = {(WIDTH){1'b0}};

  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic T_ff_sync [STAGES_SIG : 0] = '{default:{1'b0}};
  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic T_ff_ack  [STAGES_ACK : 0] = '{default:{1'b0}};

  logic T_ff_in           = 1'b0;
  logic T_ff_out          = 1'b0;
  logic out_strobe_reg    = 1'b0;
  logic input_capture;

  assign out_strobe = out_strobe_reg;
  assign bus_out    = bus_out_reg;

  assign input_capture = ~(T_ff_in ^ T_ff_ack[STAGES_ACK]);

  // capture input if transition is not active and initiate transition on input change
  always_ff @(posedge in_clk) begin
    if (input_capture && in_strobe) begin
      bus_in_stored <= bus_in;
      T_ff_in <= ~T_ff_in;
    end
  end

  // sync bus change in->out
  always_ff @(posedge out_clk) begin
    T_ff_sync[0] <= T_ff_in;
    for (int j=1; j <= STAGES_SIG; j=j+1) begin
      T_ff_sync[j] <= T_ff_sync[j-1];
    end
    T_ff_out <= T_ff_sync[STAGES_SIG];
  end

  // write output bus
  always_ff @(posedge out_clk) begin
    out_strobe_reg <= 1'b0;
    if(T_ff_out ^ T_ff_sync[STAGES_SIG]) begin
      bus_out_reg <= bus_in_stored;
      out_strobe_reg <= 1'b1;
    end
  end

  // sync ack out->in
  always_ff @(posedge in_clk) begin
    T_ff_ack[0] <= T_ff_out;
    for (int j=1; j <= STAGES_ACK; j=j+1) begin
      T_ff_ack[j] <= T_ff_ack[j-1];
    end
  end

endmodule
