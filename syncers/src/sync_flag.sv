`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Module Name: sync_flag
//
// Description: 
//   flag signal clock domain crossing with guaranteed capture of 1
//
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module sync_flag #(
    int WIDTH      = 1,
    int STAGES_SIG = 2,
    int STAGES_ACK = 2 
  )(
    input  wr_clk,
    input  rd_clk,
    input  [WIDTH-1 : 0] signal_in,  // i, sync to wr_clk 
    output [WIDTH-1 : 0] signal_out // o, sync to rd_clk
  );

logic signal_in_hold = 1'b0;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1 : 0] signal_sync [STAGES_SIG : 0] = 1'b0;
(* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1 : 0] signal_ack  [STAGES_ACK : 0] = 1'b0;

  always_ff @(posedge wr_clk)
  begin
      if (signal_in)
          signal_in_hold <= 1'b1;
      else 
          if (signal_ack[STAGES_ACK])
              signal_in_hold <= 1'b0;
          else 
              signal_in_hold <= signal_in_hold;
  end

// sync signal wr->rd
  always_ff @(posedge rd_clk) 
  begin
    signal_sync[0] <= signal_in_hold;
    for (int i=1; i <= STAGES_SIG; i=i+1)
    begin
      signal_sync[i] <= signal_sync[i-1];
    end
  end 

  assign signal_out = signal_sync[STAGES_SIG];

// sync ack rd->wr
  always_ff @(posedge wr_clk) begin
    signal_ack[0]    <= signal_sync[STAGES_SIG];
    for (int i=1; i <= STAGES_ACK; i=i+1)
    begin
      signal_ack[i] <= signal_ack[i-1];
    end
  end 

endmodule
