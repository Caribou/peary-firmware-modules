`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.09.2019 17:57:17
// Design Name: 
// Module Name: pattern_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pattern_generator #(
    int unsigned PG_PATTERN_MEM_DEPTH = 32,
    int unsigned PG_RUN_COUNT_WIDTH = 8,
    int unsigned PG_TIMER_WIDTH = 32,
    int unsigned PG_TIMEOUT_WIDTH = 32,
    int unsigned PG_SIGNALS_N = 7,
    int unsigned PG_TRIGGERS_N = 3
  )(
    input clk_cfg,
    input clk_gen,
    input rst,

    input [(PG_TRIGGERS_N)-1 : 0] triggers_in,
    output [PG_SIGNALS_N-1 : 0] outputs_out, 

    input [PG_TIMER_WIDTH-1 : 0] timer_pattern_conf_in,
    input [PG_TIMEOUT_WIDTH-1 : 0] timeout_pattern_conf_in,
    input [PG_SIGNALS_N-1 : 0] output_pattern_conf_in,
    input [(PG_TRIGGERS_N*3)+2 : 0] triggers_pattern_conf_in,
    input conf_write_pattern_in,
    output [($clog2(PG_PATTERN_MEM_DEPTH+1))-1 : 0] mem_remaining_capacity,
    input [PG_RUN_COUNT_WIDTH-1 : 0] conf_runs_n,    
    input conf_run_terminate,
    input conf_run_start,
    output stat_gen_running
  );

  logic [PG_TIMER_WIDTH-1 : 0] timer_pattern_conf;
  logic [PG_TIMEOUT_WIDTH-1 : 0] timeout_pattern_conf;
  logic [PG_SIGNALS_N-1 : 0] output_pattern_conf;
  logic [(PG_TRIGGERS_N*3)+2 : 0] triggers_pattern_conf;
  logic conf_write_pattern;
  logic [(PG_TRIGGERS_N)-1 : 0] triggers;
  
  logic [($clog2(PG_PATTERN_MEM_DEPTH))-1 : 0] mem_write_address;
  logic [($clog2(PG_PATTERN_MEM_DEPTH))-1 : 0] mem_read_address;
  logic [($clog2(PG_PATTERN_MEM_DEPTH))-1 : 0] mem_last_address;
  logic [($clog2(PG_PATTERN_MEM_DEPTH))-1 : 0] mem_last_address_Sgen;
  logic [(PG_TIMEOUT_WIDTH + PG_TIMER_WIDTH + PG_SIGNALS_N + (PG_TRIGGERS_N*3)+3)-1 : 0] mem_write_data;
  logic [(PG_TIMEOUT_WIDTH + PG_TIMER_WIDTH + PG_SIGNALS_N + (PG_TRIGGERS_N*3)+3)-1 : 0] mem_read_data;
  logic [PG_TIMER_WIDTH-1 : 0] timer_pattern_mem;
  logic [PG_TIMEOUT_WIDTH-1 : 0] timeout_pattern_mem;
  logic [PG_SIGNALS_N-1 : 0] output_pattern_mem;
  logic [PG_SIGNALS_N-1 : 0] output_pattern;
  logic [PG_SIGNALS_N-1 : 0] output_pattern_default;
  logic [(PG_TRIGGERS_N*3)+2 : 0] triggers_pattern_mem;
  logic [PG_RUN_COUNT_WIDTH-1 : 0] conf_runs_n_Sgen;
  logic conf_run_terminate_Sgen;
  logic conf_run_start_Sgen;
  logic write_default;
  logic mem_empty;
  logic rst_gen_out;
  logic arst_gen;
  logic rst_gen;
  logic gen_running;
  logic mem_write_strobe;
  logic mem_read_strobe;
  logic timer_conf_notzero;
  
  assign timer_pattern_conf    = timer_pattern_conf_in;
  assign timeout_pattern_conf  = timeout_pattern_conf_in;
  assign output_pattern_conf   = output_pattern_conf_in;
  assign triggers_pattern_conf = triggers_pattern_conf_in;
  assign conf_write_pattern    = conf_write_pattern_in;
  assign triggers              = triggers_in;
  assign timer_conf_notzero    = |timer_pattern_conf;

  assign arst_gen = rst_gen_out || rst;
  assign outputs_out = output_pattern;

  assign timer_pattern_mem    = mem_read_data[PG_TIMER_WIDTH-1 : 0];
  assign output_pattern_mem   = mem_read_data[PG_TIMER_WIDTH+PG_SIGNALS_N-1 : PG_TIMER_WIDTH];
  assign triggers_pattern_mem = mem_read_data[PG_TIMER_WIDTH+PG_SIGNALS_N+((PG_TRIGGERS_N*3)+3)-1 : PG_TIMER_WIDTH+PG_SIGNALS_N];
  assign timeout_pattern_mem  = mem_read_data[PG_TIMER_WIDTH+PG_SIGNALS_N+((PG_TRIGGERS_N*3)+3)+PG_TIMEOUT_WIDTH-1 : PG_TIMER_WIDTH+PG_SIGNALS_N+((PG_TRIGGERS_N*3)+3)];

  /*
  always_ff @(posedge clk_gen) begin
    if (mem_read_strobe) begin
      timer_pattern_mem    <= mem_read_data[TIMER_WIDTH-1 : 0];
      output_pattern_mem   <= mem_read_data[TIMER_WIDTH+SIGNALS_N-1 : TIMER_WIDTH];
      triggers_pattern_mem <= mem_read_data[TIMER_WIDTH+SIGNALS_N+((TRIGGERS_N*3)+3)-1 : TIMER_WIDTH+SIGNALS_N];
    end
    else begin
      timer_pattern_mem    <= timer_pattern_mem;
      output_pattern_mem   <= output_pattern_mem;
      triggers_pattern_mem <= triggers_pattern_mem;
    end
  end
  */
  assign mem_write_data = {timeout_pattern_conf, triggers_pattern_conf, output_pattern_conf, timer_pattern_conf};

  assign mem_remaining_capacity = (|mem_last_address) ? {1'b0, mem_write_address} + 1 : 'b0;
  // cross-domain signal syncers

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(4) 
  ) sync_rst_gen (
    .clk(clk_gen),
    .arst_in(arst_gen),  // i, async 
    .rst_out(rst_gen)   // o, sync to clk
  );

  sync_pulse #(
      .WIDTH(2),
      .STAGES(2)
  ) pulse_sync_level_gen (
      .in_clk(clk_cfg),
      .out_clk(clk_gen),
      .pulse_in({conf_run_terminate, (conf_run_start && !mem_empty)}),
      .pulse_out({conf_run_terminate_Sgen, conf_run_start_Sgen})
  );

  sync_bus #(
      .WIDTH(PG_RUN_COUNT_WIDTH),
      .STAGES(2)
  ) sync_level_conf_runs_n (
      .in_clk(clk_cfg),
      .out_clk(clk_gen),
      .bus_in(conf_runs_n),
      .bus_out(conf_runs_n_Sgen)
  );

  sync_bus #(
      .WIDTH($clog2(PG_PATTERN_MEM_DEPTH)),
      .STAGES(2)
  ) sync_level_mem_last_address (
      .in_clk(clk_cfg),
      .out_clk(clk_gen),
      .bus_in(mem_last_address),
      .bus_out(mem_last_address_Sgen)
  );

  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_level_cfg (
      .out_clk(clk_cfg),
      .signal_in(gen_running),
      .signal_out(stat_gen_running)
  );

  // submodules

  pattern_ram #(
    .RAM_WIDTH(PG_TIMEOUT_WIDTH + PG_TIMER_WIDTH + PG_SIGNALS_N + (PG_TRIGGERS_N*3)+3),
    .RAM_DEPTH(PG_PATTERN_MEM_DEPTH)   // Specify RAM depth (number of entries)
  ) pattern_ram_inst (
    .addra(mem_write_address),
    .addrb(mem_read_address),
    .dina(mem_write_data),
    .clka(clk_cfg),
    .clkb(clk_gen),
    .wea(mem_write_strobe),
    .rstb(rst_gen),
    .regceb(1'b1),
    .doutb(mem_read_data) 
  );
  
  pattern_store #(
    .PATTERN_MEM_DEPTH(PG_PATTERN_MEM_DEPTH)
  ) pattern_store_inst (
    .clk(clk_cfg),
    .rst(rst),
    .mem_write_strobe(mem_write_strobe),
    .mem_write_address(mem_write_address),
    .mem_last_address(mem_last_address),
    .timer_notzero(timer_conf_notzero),
    .conf_write_pattern(conf_write_pattern),
    .reset_generator(rst_gen_out),
    .write_default(write_default),
    .mem_empty(mem_empty)
  );
  
  always_ff @(posedge clk_cfg) begin
    if (rst)
      output_pattern_default <= {PG_SIGNALS_N{1'b0}};
    else if (write_default)
      output_pattern_default <= output_pattern_conf;
  end
    
  pattern_load #(
    .PATTERN_MEM_DEPTH(PG_PATTERN_MEM_DEPTH),
    .RUN_COUNT_WIDTH(PG_RUN_COUNT_WIDTH),
    .TIMER_WIDTH(PG_TIMER_WIDTH),
    .TIMEOUT_WIDTH(PG_TIMEOUT_WIDTH),
    .SIGNALS_N(PG_SIGNALS_N),
    .TRIGGERS_N(PG_TRIGGERS_N)
  ) pattern_load_inst (
    .clk(clk_gen),
    .rst(rst_gen),
  
    .mem_read_strobe(mem_read_strobe),
    .mem_read_address(mem_read_address),
    .mem_last_address(mem_last_address_Sgen),
    
        
    .signals_pattern_mem(output_pattern_mem),
    .signals_out(output_pattern),
    .signals_default(output_pattern_default),
    
    .timer_pattern_mem(timer_pattern_mem),
    .timeout_pattern_mem(timeout_pattern_mem),
    .triggers_pattern_mem(triggers_pattern_mem),
    .triggers_in(triggers),
    .conf_runs_n(conf_runs_n_Sgen),
    .conf_run_terminate(conf_run_terminate_Sgen),
    .conf_run_start(conf_run_start_Sgen),
  
    .running(gen_running)
  );
  
/*
  ila_0 ila_gen_cfg (
    .clk(clk_cfg), // input wire clk
    .probe0(mem_write_address), // input wire [4:0]  probe0  
    .probe1(mem_last_address), // input wire [4:0]  probe1 
    .probe2(timer_pattern_conf), // input wire [31:0]  probe2 
    .probe3(timer_conf_notzero), // input wire [0:0]  probe3 
    .probe4(conf_write_pattern), // input wire [0:0]  probe4 
    .probe5(mem_write_strobe), // input wire [0:0]  probe5 
    .probe6(conf_run_terminate), // input wire [0:0]  probe6 
    .probe7(conf_run_start) // input wire [0:0]  probe7 
  );
 
  ila_1 ila_gen_run (
    .clk(clk_gen), // input wire clk
    .probe0(mem_read_address), // input wire [4:0]  probe0  
    .probe1(timer_pattern_mem), // input wire [31:0]  probe1 
    .probe2(rst_gen), // input wire [0:0]  probe2 
    .probe3(mem_read_strobe), // input wire [0:0]  probe3 
    .probe4(conf_run_start_Sgen), // input wire [0:0]  probe4 
    .probe5(conf_run_terminate_Sgen), // input wire [0:0]  probe5 
    .probe6(gen_running) // input wire [0:0]  probe6 
  );
*/
endmodule