`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Design Name: Pattern Generator 
// Module Name: pattern_load
// Project Name: Caribou
// Target Devices: Caribou
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pattern_load #(
    int unsigned PATTERN_MEM_DEPTH = 32,
    int unsigned RUN_COUNT_WIDTH = 8,
    int unsigned TIMER_WIDTH = 32,
    int unsigned TIMEOUT_WIDTH = 32,
    int unsigned SIGNALS_N = 7,
    int unsigned TRIGGERS_N = 2
)(
    input clk,
    input rst,

    output mem_read_strobe,
    output [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_read_address,
    input [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_last_address,
    
    input  [SIGNALS_N-1 : 0] signals_pattern_mem,
    input  [SIGNALS_N-1 : 0] signals_default,
    output [SIGNALS_N-1 : 0] signals_out,
    
    input [TIMER_WIDTH-1 : 0] timer_pattern_mem,
    input [TIMEOUT_WIDTH-1 : 0] timeout_pattern_mem,
    input [(TRIGGERS_N*3)+2 : 0] triggers_pattern_mem,
    input [(TRIGGERS_N)-1 : 0] triggers_in,
    input [RUN_COUNT_WIDTH-1 : 0] conf_runs_n,
    input conf_run_terminate,
    input conf_run_start,

    output running
);

  // control
  logic gen_start;
  logic gen_start_hold;
  logic gen_running;

  // signals
  logic [SIGNALS_N-1 : 0] signals_next;
  logic [SIGNALS_N-1 : 0] signals;
  logic signals_load;

  //timer
  logic [TIMER_WIDTH-1 : 0] timer_pattern_next;
  logic [TIMEOUT_WIDTH-1 : 0] timeout_pattern_next;
  logic [(TRIGGERS_N*3)+2 : 0] triggers_pattern_next;
  
  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] pattern_mem_ptr; 
  logic last_pattern_mem;
  logic last_pattern_next;  
  logic last_pattern;  
  
  logic [TIMER_WIDTH-1 : 0] timer;
  logic timer_isone;
  logic load_next_pattern;
    
  logic [RUN_COUNT_WIDTH-1 : 0] runs_count;
  logic last_run;

  logic [(TRIGGERS_N)-1 : 0] triggers_prev;
  logic [(TRIGGERS_N)-1 : 0] triggers;
  logic trigger_global;

  logic [TIMEOUT_WIDTH-1 : 0] timeout;
  logic timeout_isone;
  logic timeout_count_prev;
  logic timeout_count;
  logic timeout_start;
  
  logic terminate_request;
  
  assign signals_out = signals;
  assign mem_read_address = pattern_mem_ptr;
  assign running = gen_running || gen_start_hold || gen_start;
  assign mem_read_strobe = load_next_pattern;

  assign signals_load = load_next_pattern;
    
  // output signals
  always_ff @(posedge clk) begin
    if (rst)
      signals <= signals_default;
    else if (signals_load)
      signals <= signals_next;
    else 
      signals <= signals;
  end

  assign signals_next          = signals_pattern_mem;
  assign timer_pattern_next    = timer_pattern_mem;
  assign timeout_pattern_next  = timeout_pattern_mem;
  assign triggers_pattern_next = triggers_pattern_mem;
  assign last_pattern_next     = last_pattern_mem;
  assign gen_start             = conf_run_start;
  
  /*   
  // if needed memory load register
  always_ff @(posedge clk) begin
    if (rst) begin
      signals_next          <= 'b0;
      timer_pattern_next    <= 'b0;
      timeout_pattern_next  <= 'b0;
      triggers_pattern_next <= 'b0;
      last_pattern_next     <= 1'b0;
      gen_start             <= 1'b0;
    end
    else begin
      gen_start             <= conf_run_start
      if (load_next_pattern || rst_prev) begin
        signals_next          <= signals_pattern_mem;
        timer_pattern_next    <= timer_pattern_mem;
        timeout_pattern_next  <= timeout_pattern_mem;
        triggers_pattern_next <= triggers_pattern_mem;
        last_pattern_next     <= last_pattern_mem;
      end
      else begin 
        signals_next          <= signals_next;
        timer_pattern_next    <= timer_pattern_next;
        timeout_pattern_next  <= timeout_pattern_next;
        triggers_pattern_next <= triggers_pattern_next;
        last_pattern_next     <= last_pattern_next;
      end
    end
  end
  */

  // ///////////////////////
  // /// trigger signals ///
  // ///////////////////////
  //
  // configuration of individual signals:
  // H High       100
  // L Low        101
  // R Rising     001
  // F Falling    010
  // E Edge (any) 011
  // A Always     111
  // N Never      000
  // X reserved   110
  //
  // configuration of global trigger condition:
  //   OR 001
  //  NOR 101
  //  AND 011
  // NAND 111
  //  XOR 010
  // XNOR 110
  //  '1' 000
  // --   100
  //
  
  
  // capture previous state
  always_ff @(posedge clk)
    triggers_prev <= triggers_in;
  
  generate
    // individual triggers based on their settings
    for (genvar i=0; i < TRIGGERS_N; i=i+1) begin
      always_comb begin
        case (triggers_pattern_next[((3*i)+2)+3 : ((3*i))+3])
          // High
          3'b100 : begin
            if (triggers_in[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Low
          3'b101 : begin
            if (~triggers_in[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Rising
          3'b001 : begin
            if (triggers_in[i] & ~triggers_prev[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Falling
          3'b010 : begin
            if (~triggers_in[i] & triggers_prev[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Edge (any)
          3'b011 : begin
            if (triggers_in[i] ^ triggers_prev[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Always
          3'b111 : begin
            triggers[i] = 1'b1;
          end
  
          // Never
          3'b000 : begin
            triggers[i] = 1'b0;
          end
  
          // Reserved
          3'b110 : begin
            triggers[i] = 1'b0;
          end
  
          default : begin
            triggers[i] = 1'b0;
          end
          
        endcase
      end
    end
  endgenerate
  
  // global trigger condition
  always_comb begin
    case (triggers_pattern_next[2:0])
        // OR
        3'b001 : begin
          trigger_global = |triggers;
        end

        // NOR
        3'b101 : begin
          trigger_global = ~|triggers;
        end

        // AND
        3'b011 : begin
          trigger_global = &triggers;
        end

        // NAND
        3'b111 : begin
          trigger_global = ~&triggers;
        end

        // XOR
        3'b010 : begin
          trigger_global = ^triggers;
        end

        // XNOR
        3'b110 : begin
          trigger_global = ~^triggers;
        end

        // always armed
        3'b000 : begin
          trigger_global = 1'b1;
        end

        // Reserved
        3'b100 : begin
          trigger_global = 1'b0;
        end

        default : begin
          trigger_global = 1'b0;
        end

      endcase
  end

  // //////////////////////
  // /// next pattern   ///
  // /// memory pointer ///
  // //////////////////////

  // note that the pointer starts from the end and counts down
  always_comb begin
    // pattern mem pointer is at the end
    if ( pattern_mem_ptr == mem_last_address )
      last_pattern_mem = 1'b1;
    else 
      last_pattern_mem = 1'b0;
  end  

  always_ff @(posedge clk) begin
    if (rst)
      last_pattern <= 1'b0; 
    else if (load_next_pattern || gen_start_hold)
      last_pattern <= last_pattern_next;
    else  
      last_pattern <= last_pattern;  
  end
  
  // pattern mem pointer
  always_ff @(posedge clk) begin
    if (rst || gen_start || (last_pattern_mem && load_next_pattern))
      pattern_mem_ptr <= PATTERN_MEM_DEPTH-1;
    else if (load_next_pattern)
      pattern_mem_ptr <= pattern_mem_ptr - 1;
    else
      pattern_mem_ptr <= pattern_mem_ptr;
  end  

  assign load_next_pattern = gen_running && timer_isone && !(last_run && last_pattern) && (trigger_global || timeout_isone);
  assign timeout_count     = gen_running && timer_isone && !(last_run && last_pattern) && !trigger_global;
  
  // ///////////////////////
  // /// trigger timeout ///
  // ///////////////////////
    
  always_ff @(posedge clk)
    timeout_count_prev <= timeout_count;
    
  assign timeout_start = !timeout_count_prev && timeout_count;
  
  // pattern timeout != 1 
  always_comb begin
    if ( ~|timeout[TIMEOUT_WIDTH-1:1] && timeout[0])
      timeout_isone = 1'b1;
    else 
      timeout_isone = 1'b0;
  end  

  // trigger timeout
  always_ff @(posedge clk) begin
    if (rst || gen_start_hold)
      timeout <= {TIMEOUT_WIDTH{1'b0}};
    else if (timeout_start)
      timeout <= timeout_pattern_next;
    else if (|timeout)
      timeout <= timeout - 1;
    else
      timeout <= timeout;
  end
  
  // //////////////////////////
  // /// pattern timer loop ///
  // //////////////////////////

  // pattern timer is <= 1 
  always_comb begin
    if ( ~|timer[TIMER_WIDTH-1:1] )
      timer_isone = 1'b1;
    else 
      timer_isone = 1'b0;
  end  

  // pattern timer
  always_ff @(posedge clk) begin
    if (rst || gen_start_hold)
      timer <= {TIMER_WIDTH{1'b0}};
    else if (load_next_pattern)
      timer <= timer_pattern_next;
    else if (!timer_isone)
      timer <= timer - 1;
    else
      timer <= timer;
  end
 
  // ///////////////////   
  // /// run counter /// 
  // ///////////////////   

  // last run
  always_comb begin
    // request to stop
    if (terminate_request)
      last_run = 1'b1;
    // if conf_runs == 0, run forever
    else if (~|conf_runs_n)
      last_run = 1'b0;
    else if (runs_count == conf_runs_n)
      last_run = 1'b1;
    else 
      last_run = 1'b0;
  end

  // run no. counter
  always_ff @(posedge clk) begin
    if (rst || gen_start_hold)
      runs_count <= 1;
    else if (load_next_pattern && last_pattern)
      runs_count <= runs_count + 1;
    else
      runs_count <= runs_count;
  end

  // ///////////////////
  // /// run control ///
  // ///////////////////
  
  
  always_ff @(posedge clk) begin
    if (rst) begin
      gen_start_hold <= 1'b0;
      gen_running    <= 1'b0;
    end
    else if (conf_run_start || gen_start) begin
      gen_start_hold <= 1'b1;
      gen_running    <= 1'b0;
    end
    else if (gen_start_hold) begin
      gen_start_hold <= 1'b0;
      gen_running    <= 1'b1;
    end
    else if (timer_isone && last_run && last_pattern) begin
      gen_start_hold <= 1'b0;
      gen_running    <= 1'b0;
    end
    else begin
      gen_start_hold <= 1'b0;
      gen_running    <= gen_running;
    end
  end

  always_ff @(posedge clk) begin
    if (!gen_running)
      terminate_request <= 1'b0;
    else if (conf_run_terminate)
      terminate_request <= 1'b1;
    else
      terminate_request <= terminate_request;
  end

  /*
  ila_3 ila_gen_load (
        .clk(clk), // input wire clk
        .probe0(mem_read_address), // input wire [4:0]  probe0  
        .probe1(last_pattern_mem), // input wire [0:0]  probe1 
        .probe2(signals_next), // input wire [7:0]  probe2 
        .probe3(timer_pattern_next), // input wire [31:0]  probe3 
        .probe4(triggers_pattern_next), // input wire [11:0]  probe4 
        .probe5(conf_run_start), // input wire [0:0]  probe5 
        .probe6(gen_start_hold), // input wire [0:0]  probe6 
        .probe7(gen_start), // input wire [0:0]  probe7
        .probe8(gen_running), // input wire [0:0]  probe
        .probe9(conf_run_terminate), // input wire [0:0]  probe
        .probe10(terminate_request), // input wire [0:0]  probe
        .probe11(load_next_pattern), // input wire [0:0]  probe
        .probe12(signals), // input wire [7:0]  probe
        .probe13(last_pattern), // input wire [0:0]  probe
        .probe14(last_run), // input wire [0:0]  probe
        .probe15(trigger_global), // input wire [0:0]  probe
        .probe16(timer_isone), // input wire [0:0]  probe
        .probe17(timer), // input wire [31:0]  probe
        .probe18(runs_count), // input wire [31:0]  probe
        .probe19(triggers_in), // input wire [2:0]  probe
        .probe20(timeout_pattern_next), // input wire [31:0]  probe
        .probe21(timeout), // input wire [31:0]  probe
        .probe22(timeout_isone), // input wire [0:0]  probe
        .probe23(timeout_count_prev), // input wire [0:0]  probe
        .probe24(timeout_count), // input wire [0:0]  probe
        .probe25(timeout_start) // input wire [0:0]  probe
    );
    */

endmodule
