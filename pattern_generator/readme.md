# Pattern generator
Pattern generator can be used to generate output signals according to a set of rules.

## Function

Each combination of output signals is called a **pattern**. A list of patterns, as they come in time, is defined in a memory. Each pattern has following parameters: time, trigger timeout, trigger and the output state. A pattern is loaded when
*the time of the previous pattern has passed*
AND (
*trigger condition of the current pattern is satisfied*
OR
*trigger timeout of the current pattern has passed*
).

After the generator goes through all patterns in the list, it either starts over or stops at the last pattern, depending on the configuration. Going through a list of all patterns once is called a **run**.

All time settings is in units of `clk_gen` clock cycles.


## Pattern generator generics:

* **PG_PATTERN_MEM_DEPTH** pattern memory capacity (number of patterns that can be stored)

* **PG_RUN_COUNT_WIDTH** width of a counter that counts performed runs and also of the configuration register that defines the number of runs to be performed.

* **PG_TIMER_WIDTH** width of a pattern parameter specifying the time for which the pattern is present at the output

* **PG_TIMEOUT_WIDTH** width of a pattern parameter specifying the time for which the current pattern wait for a trigger condition

* **PG_SIGNALS_N** number of output signals

* **PG_TRIGGERS_N** number of trigger inputs


## Pattern generator connections:

* **clk_cfg**: input, clock for configuration interface

* **clk_gen**: input, clock for pattern timing

* **rst**: input, active-high reset, sync to `clk_cfg`

   Storing patterns is available immediately after reset goes back to `0`. Running the generator after the reset is released is possible only after 2 `clk_cfg` cycles followed by 6 `clk_gen` cycles.

* **triggers_in** _[PG_TRIGGERS_N]_: input, generator trigger inputs, sync to `clk_gen`


* **timer_pattern_conf_in** _[PG_TIMER_WIDTH]_: input, pattern configuration: pattern time, sync to `clk_cfg`

  Number of `clk_gen` clock cycles for which the pattern will be active

* **timeout_pattern_conf_in** _[PG_TIMEOUT_WIDTH]_: input, pattern configuration: pattern trigger timeout, sync to `clk_cfg`

  Number of `clk_gen` clock cycles for which the generator will wait for the trigger condition. Until the trigger condition is not met, the pattern is not loaded and the previous state of outputs is kept.
  If set to `0` the timeout will not be applied and the pattern will be loaded on trigger condition only

* **output_pattern_conf_in** _[PG_SIGNALS_N]_: input, pattern configuration: output pattern, sync to `clk_cfg`

* **triggers_pattern_conf_in** _[(PG_TRIGGERS_N*3)+3]_: input, pattern configuration: pattern trigger condition, sync to `clk_cfg`

  See below for a detailed trigger description

* **conf_write_pattern_in**: input, sync to `clk_cfg`

  When the signal is `1`, the state of pattern configuration inputs (`*_pattern_conf_in`) is pushed to a next position in the pattern memory

* **conf_runs_n** _[PG_RUN_COUNT_WIDTH]_: input, parameter to configure number of runs to be performed, sync to `clk_cfg`, internally synced to `clk_gen`, thus accepting the change is delayed by approximately 2 `clk_cfg` cycles followed by 6 `clk_gen` cycles

  If set to 0, the generator will run forever until stopped manually.
  The desired value has to be kept at the input all the time the generator runs. There is no internal register to store it.

* **conf_run_terminate**: input, generator stop, sync to `clk_cfg`

  Setting signal to `1` will stop the generator after the current run is finished. Setting `1` for one clock cycle is enough. The signal sets an internal flag which is kept until the end of a run.

* **conf_run_start**: input, generator start, sync to `clk_cfg`

  Setting signal to `1` for one clock period starts the generator. The generator will not start if no patterns are set in the memory.
  Setting the signal to `1` while the generator is running will restart the generator.

* **mem_remaining_capacity** _[$clog2(PG_PATTERN_MEM_DEPTH+1)]_: output, sync to `clk_cfg`

  Output signalizing the number of remaining available positions in pattern memory.


* **outputs_out** _[PG_SIGNALS_N]_: output, generator outputs, sync to `clk_gen`

* **stat_gen_running**: output, generator running flag, sync to `clk_cfg` - internally synced from `clk_gen` domain, thus is delayed by a few clk cycles.

  status of the generator. If `1`, the generator is running, `0` otherwise.


## Trigger conditions:

The trigger configuration for each pattern has 3 bits for each trigger input and 3 bits in addition for a global condition.

The lower 3 bits (`triggers_pattern_conf_in[2:0]`) are used for the global condition.
Each 3 following bits are used for a trigger condition of a corresponding trigger signal:

`triggers_pattern_conf_in[5:3]` is for `triggers_in[0]`,

`triggers_pattern_conf_in[8:6]` is for `triggers_in[1]` etc.


Individual trigger signal condition can be set to following values:
```
H High       100
L Low        101
R Rising     001
F Falling    010
E Edge (any) 011
A Always     111
N Never      000
X reserved   110
```
The global trigger condition can be set to following values:
```
         OR 001
        NOR 101
        AND 011
       NAND 111
        XOR 010
       XNOR 110
always true 000
   reserved 100
```
Until the trigger condition of the current pattern is not satisfied, previous output state remains active.

Next pattern is loaded when a global trigger condition evaluates to true. If the global trigger condition is set to `000`, it is always evaluated to true regardless the status of individual trigger signals. Otherwise the global trigger status is evaluated as a corresponding logic operation applied on all individual trigger statuses.

An individual trigger signal status is true, if the status of the signal corresponds to the selected condition. If set to Always (`111`) it is always true, if set to Never (`000`) the corresponding signal never meets the trigger condition and the signal's trigger status is always false.

Example `triggers_pattern_conf_in` configuration for a generator with 3 trigger inputs:

* `xxxxxxxxx000`: trigger is always true, the pattern is loaded immediately
* `100000001001`: trigger if `triggers_in[2]=='1'` OR `triggers_in[0]` changed state from `0` to `1`
* `111101011011`: trigger if `triggers_in[1]=='0'` AND `triggers_in[0]` changed in any direction
* `000xxxxxx011`: this condition can never be met, since `triggers_in[2]` condition is set to never thus the individual trigger is always false and the global condition is AND, meaning that all individual triggers have to be true.


## Special conditions:

* Writing a pattern with `timer_pattern_conf_in == 0` resets the pattern memory and at the same time sets the output value to the value that is present at `output_pattern_conf_in`.

* Writing a pattern when no space is left in the pattern memory (`mem_remaining_capacity == 0`) has no effect.

* A pattern with `timeout_pattern_conf_in == 0` will have no timeout when waiting for a trigger.

* Setting `conf_runs_n` to `0` will keep the generator running forever until it is stopped manually.

* A pattern with a trigger condition that is always valid will be loaded immediately after the previous pattern times out (as one would expect).


## Operation example:

1. Reset the generator.

   **OR**

   Set `timer_pattern_conf_in` to `0` and `output_pattern_conf_in` to a desired starting value and set `conf_write_pattern_in` to `1` for one clock cycle.

* The pattern memory is now empty and ready to store pattern data.

2. Set the `*_pattern_conf_in` inputs to desired values for the first (next) pattern.

3. Set `conf_write_pattern_in` to `1` for one clock cycle. This will push the pattern configuration to a memory. Having the signal high for more clock cycles will push another pattern with each clock cycle.

* Setting the `*_pattern_conf_in` inputs and `conf_write_pattern_in` can be done in one clock cycle. There is no need to have the `*_pattern_conf_in` inputs valid before.

4. Repeat steps 2 and 3 until all patterns are stored or the pattern memory is full.

5. Set the `conf_runs_n` value to the desired value and wait a few clock cycles to propagate the change to `clk_gen` domain.

* The generator is now configured

6. Set `conf_run_start` to `1` for one clock cycle. This will start the generator.

7. The generator will stop after the configured number of runs, or at the end of the run during which the `conf_run_terminate` was set to `1`. Immediate stop is possible by resetting the generator only.
