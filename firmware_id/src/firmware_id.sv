
module firmware_id
  (
    output [31 : 0] firmware_id_sig
   );

    USR_ACCESSE2 firmware_id_reg (
      .CFGCLK(),              // Not utilized in the static use case
      .DATA(firmware_id_sig), // 32-bit output Configuration Data output
      .DATAVALID()            // Not utilized in the static use case
    );
    
endmodule 


