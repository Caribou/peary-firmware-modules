/*

    Single-ended output signal and clock buffer.

*/

module xil_obuf #(
    parameter DRIVE      = 12, // Specify the output drive strength
    parameter IOSTANDARD = "DEFAULT",
    parameter SLEW       = "SLOW"
) (
    input I,
    output O
);

// OBUF: Single-ended Output Buffer
// 7 Series
// Xilinx HDL Libraries Guide, version 2012.2
OBUF #(
    .DRIVE(DRIVE), 
    .IOSTANDARD(IOSTANDARD), 
    .SLEW(SLEW) 
) OBUF_inst (
    .O(O), // Buffer output (connect directly to top-level port)
    .I(I) // Buffer input
);
// End of OBUF_inst instantiation

endmodule