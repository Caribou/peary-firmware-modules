`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.08.2020 10:08:41
// Design Name: 
// Module Name: xil_obufds_bus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module xil_obufds_bus # (
    parameter IOSTANDARD = "DEFAULT",
    parameter SLEW       = "SLOW",
    parameter WIDTH      = 8
)
(
    input wire [WIDTH-1:0] I,
    output wire [WIDTH-1:0] O_p,
    output wire [WIDTH-1:0] O_n
    );
   
   OBUFDS #(
      .IOSTANDARD(IOSTANDARD), // Specify the output I/O standard
      .SLEW(SLEW)              // Specify the output slew rate
   ) OBUFDS_inst[WIDTH-1:0] (
      .O(O_p),     // Diff_p output (connect directly to top-level port)
      .OB(O_n),   // Diff_n output (connect directly to top-level port)
      .I(I)      // Buffer input 
   );

endmodule
