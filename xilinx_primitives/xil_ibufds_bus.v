`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.08.2020 10:08:41
// Design Name: 
// Module Name: xil_ibufds_bus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module xil_ibufds_bus # (
    parameter IOSTANDARD = "DEFAULT",
    parameter SLEW       = "SLOW",
    parameter WIDTH      = 8
)
(
    input wire [WIDTH-1:0] I_p,
    input wire [WIDTH-1:0] I_n,
    output wire [WIDTH-1:0] O
    );
   
   OBUFDS #(
      .IOSTANDARD(IOSTANDARD), // Specify the output I/O standard
      .SLEW(SLEW)              // Specify the output slew rate
   ) OBUFDS_inst[WIDTH-1:0] (
      .I(I_p),     // Diff_p input (connect directly to top-level port)
      .IB(I_n),   // Diff_n input (connect directly to top-level port)
      .O(O)      // Buffer output 
   );

endmodule
