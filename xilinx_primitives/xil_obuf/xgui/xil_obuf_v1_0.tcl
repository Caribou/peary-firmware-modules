# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "DRIVE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "IOSTANDARD" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SLEW" -parent ${Page_0}


}

proc update_PARAM_VALUE.DRIVE { PARAM_VALUE.DRIVE } {
	# Procedure called to update DRIVE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DRIVE { PARAM_VALUE.DRIVE } {
	# Procedure called to validate DRIVE
	return true
}

proc update_PARAM_VALUE.IOSTANDARD { PARAM_VALUE.IOSTANDARD } {
	# Procedure called to update IOSTANDARD when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IOSTANDARD { PARAM_VALUE.IOSTANDARD } {
	# Procedure called to validate IOSTANDARD
	return true
}

proc update_PARAM_VALUE.SLEW { PARAM_VALUE.SLEW } {
	# Procedure called to update SLEW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SLEW { PARAM_VALUE.SLEW } {
	# Procedure called to validate SLEW
	return true
}


proc update_MODELPARAM_VALUE.DRIVE { MODELPARAM_VALUE.DRIVE PARAM_VALUE.DRIVE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DRIVE}] ${MODELPARAM_VALUE.DRIVE}
}

proc update_MODELPARAM_VALUE.IOSTANDARD { MODELPARAM_VALUE.IOSTANDARD PARAM_VALUE.IOSTANDARD } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IOSTANDARD}] ${MODELPARAM_VALUE.IOSTANDARD}
}

proc update_MODELPARAM_VALUE.SLEW { MODELPARAM_VALUE.SLEW PARAM_VALUE.SLEW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SLEW}] ${MODELPARAM_VALUE.SLEW}
}

