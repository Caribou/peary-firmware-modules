`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.07.2019 09:58:13
// Design Name: 
// Module Name: xil_bufgmux_ctrl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module xil_bufgmux_ctrl (
    input  I0,
    input  I1,
    input  S,
    output O
    );

    // BUFGMUX_CTRL: 2-to-1 Global Clock MUX Buffer
    // 7 Series
    // Xilinx HDL Libraries Guide, version 2012.2
    BUFGMUX_CTRL BUFGMUX_CTRL_inst (
        .O(O), // 1-bit output: Clock output
        .I0(I0), // 1-bit input: Clock input (S=0)
        .I1(I1), // 1-bit input: Clock input (S=1)
        .S(S) // 1-bit input: Clock select
    );
    // End of BUFGMUX_CTRL_inst instantiation


endmodule
