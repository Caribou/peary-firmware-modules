
module Caribou_control  #(
    // Width of S_AXI data bus
    parameter integer C_S_AXI_DATA_WIDTH  = 32,
    // Width of S_AXI address bus
    parameter integer C_S_AXI_ADDR_WIDTH  = 12
  )(
    // Ports of Axi4Lite Slave Bus Interface S00_AXI
    input   S_AXI_ACLK,
    input   S_AXI_ARESETN,
    input  [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
    input  [2 : 0] S_AXI_AWPROT,
    input   S_AXI_AWVALID,
    output  S_AXI_AWREADY,
    input  [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
    input  [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
    input   S_AXI_WVALID,
    output  S_AXI_WREADY,
    output [1 : 0] S_AXI_BRESP,
    output  S_AXI_BVALID,
    input   S_AXI_BREADY,
    input  [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
    input  [2 : 0] S_AXI_ARPROT,
    input   S_AXI_ARVALID,
    output  S_AXI_ARREADY,
    output [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
    output [1 : 0] S_AXI_RRESP,
    output  S_AXI_RVALID,
    input   S_AXI_RREADY
  );

  /////////////////////////
  // AXI BUS             //
  /////////////////////////

  localparam AXI_DATA_BYTES = ((C_S_AXI_DATA_WIDTH-1)/8)+1;
  localparam AXI_ADDR_LSB = ($clog2(C_S_AXI_DATA_WIDTH)-3);
  localparam AXI_REG_ADDR_WIDTH = C_S_AXI_ADDR_WIDTH-AXI_ADDR_LSB;

  logic [C_S_AXI_DATA_WIDTH-1:0] axi_mem_rdata;
  logic [C_S_AXI_DATA_WIDTH-1:0] axi_mem_wdata;
  logic [AXI_REG_ADDR_WIDTH-1:0]  axi_mem_rdAddr;
  logic [AXI_REG_ADDR_WIDTH-1:0]  axi_mem_wrAddr;
  logic [AXI_DATA_BYTES-1:0] axi_mem_wrByteStrobe;
  logic  axi_mem_rdStrobe;

  axi4lite_slave_interface #(
    .C_S_AXI_DATA_WIDTH (C_S_AXI_DATA_WIDTH),
    .C_S_AXI_ADDR_WIDTH (C_S_AXI_ADDR_WIDTH)
  ) axi4_slave_inst (

    .axi_mem_rdata(axi_mem_rdata),
    .axi_mem_wdata(axi_mem_wdata),
    .axi_mem_rdAddr(axi_mem_rdAddr),
    .axi_mem_wrAddr(axi_mem_wrAddr),
    .axi_mem_wrByteStrobe(axi_mem_wrByteStrobe),
    //.axi_mem_rdEnable(axi_mem_rdEnable),
    .axi_mem_rdStrobe(axi_mem_rdStrobe),

    .S_AXI_ACLK(S_AXI_ACLK),
    .S_AXI_ARESETN(S_AXI_ARESETN),
    .S_AXI_AWADDR(S_AXI_AWADDR),
    .S_AXI_AWPROT(S_AXI_AWPROT),
    .S_AXI_AWVALID(S_AXI_AWVALID),
    .S_AXI_AWREADY(S_AXI_AWREADY),
    .S_AXI_WDATA(S_AXI_WDATA),
    .S_AXI_WSTRB(S_AXI_WSTRB),
    .S_AXI_WVALID(S_AXI_WVALID),
    .S_AXI_WREADY(S_AXI_WREADY),
    .S_AXI_BRESP(S_AXI_BRESP),
    .S_AXI_BVALID(S_AXI_BVALID),
    .S_AXI_BREADY(S_AXI_BREADY),
    .S_AXI_ARADDR(S_AXI_ARADDR),
    .S_AXI_ARPROT(S_AXI_ARPROT),
    .S_AXI_ARVALID(S_AXI_ARVALID),
    .S_AXI_ARREADY(S_AXI_ARREADY),
    .S_AXI_RDATA(S_AXI_RDATA),
    .S_AXI_RRESP(S_AXI_RRESP),
    .S_AXI_RVALID(S_AXI_RVALID),
    .S_AXI_RREADY(S_AXI_RREADY)
  );

  /////////////////////////
  // AXI FPGA REGISTERS  //
  /////////////////////////

  // number of FPGA registers
  localparam int unsigned REGISTER_N = 4;
  // FPGA register mapping
  localparam byte unsigned  REG_FW_VERSION = 0;
  localparam byte unsigned  REG_R1         = 1;
  localparam byte unsigned  REG_R2         = 2;
  localparam byte unsigned  REG_R3         = 3;

  logic axi_wren_regs;
  logic axi_rden_regs;
  logic [C_S_AXI_DATA_WIDTH-1:0]       regs_dout;
  logic [((C_S_AXI_DATA_WIDTH-1)/8):0] regs_wrByteStrobe [REGISTER_N-1:0];
  logic                                regs_rdStrobe     [REGISTER_N-1:0];
  logic [C_S_AXI_DATA_WIDTH-1:0]       regs_din          [REGISTER_N-1:0];

  assign axi_wren_regs = 1'b1;
  assign axi_rden_regs = 1'b1;

  // registers demultiplexer
  mem_regs #(
    .REGISTER_N(REGISTER_N),
    .REG_DATA_WIDTH(C_S_AXI_DATA_WIDTH)
  ) caribou_regs (
    // REG INTERFACE:
    .reg_wrdout(regs_dout),
    .reg_wrByteStrobe(regs_wrByteStrobe),
    .reg_rdStrobe(regs_rdStrobe),
    .reg_rddin(regs_din),
    // MEM INTERFACE
    .mem_wrSelect(axi_wren_regs),
    .mem_rdSelect(axi_rden_regs),
    .mem_rddout(axi_mem_rdata),
    .mem_wrdin(axi_mem_wdata),
    .mem_rdAddr(axi_mem_rdAddr[$clog2(REGISTER_N)-1:0]),
    .mem_wrAddr(axi_mem_wrAddr[$clog2(REGISTER_N)-1:0]),
    .mem_wrByteStrobe(axi_mem_wrByteStrobe),
    .mem_rdStrobe(axi_mem_rdStrobe)
  );

  logic [31:0] fw_version;
  logic [31:0] r1;
  logic [31:0] r2;
  logic [31:0] r3;

  USR_ACCESSE2 firmware_version (
    .CFGCLK(), // Not utilized in the static use case
    .DATA(fw_version), // 32-bit output
    .DATAVALID() // Not utilized in the static use case
  );

  // REG_FW_VERSION
  assign regs_din[REG_FW_VERSION][31:0] = fw_version;

  // REG_R1
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      r1 <=  32'b0;
    end
    else begin
      for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
        if ( regs_wrByteStrobe[REG_R1][byte_index] == 1 )
          r1[(byte_index*8) +: 8] <= regs_dout[(byte_index*8) +: 8];
    end
  end
  assign regs_din[REG_R1][31:0] = r1;

  // REG_R2
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      r2 <=  32'b0;
    end
    else begin
      for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
        if ( regs_wrByteStrobe[REG_R2][byte_index] == 1 )
          r2[(byte_index*8) +: 8] <= regs_dout[(byte_index*8) +: 8];
    end
  end
  assign regs_din[REG_R2][31:0] = r2;

  // REG_R3
  always_ff @(posedge S_AXI_ACLK) begin
    if (~S_AXI_ARESETN) begin
      r3 <=  32'b0;
    end
    else begin
      for (byte unsigned byte_index = 0; byte_index <= AXI_DATA_BYTES-1; byte_index = byte_index+1 )
        if ( regs_wrByteStrobe[REG_R3][byte_index] == 1 )
          r3[(byte_index*8) +: 8] <= regs_dout[(byte_index*8) +: 8];
    end
  end
  assign regs_din[REG_R3][31:0] = r3;

endmodule // Caribou_control
