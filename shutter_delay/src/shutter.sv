`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.08.2019 13:33:01
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shuttercontrol(
    input clk,
    input reset,
    input start,
    input [31:0] timeout,
    output shutter
  );
  
  logic [31:0] counter;
  logic counter_set;
  logic shutter_i;
  

  assign counter_set = |counter;
  assign shutter = shutter_i;
  
  always_ff @(posedge clk) begin
    if (reset) begin
      counter <= 'b0;
    end
    else if ( start ) begin
      counter <= timeout;
    end
    else if ( counter_set ) begin
      counter <= counter-1;
    end
    else begin
      counter <= 'b0;
    end
  end  

  always_ff @(posedge clk) begin
    shutter_i <= counter_set;
  end
  
endmodule